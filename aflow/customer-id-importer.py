from airflow.decorators import dag, task

from datetime import datetime
from datetime import timedelta

from airflow.decorators import dag, task

DEFAULT_ARGS = {
    "owner": "meve",
    "depends_on_past": False,
    "email": "labs@crossref.org",
    "email_on_failure": False,
    "email_on_retry": False,
}


@dag(
    default_args=DEFAULT_ARGS,
    schedule_interval="@weekly",
    catchup=False,
    dagrun_timeout=timedelta(days=3),
    start_date=datetime(2023, 1, 29),
    tags=["API", "admin"],
)
def import_customer_ids():
    @task.virtualenv(
        task_id="import_customers",
        requirements=[
            "claws",
            "bs4",
            "lxml",
            "python-slugify",
            "smart_open",
            "requests",
            "urllib3",
        ],
        system_site_packages=True,
    )
    def import_customers():
        def _get_member_data_file():
            import requests
            from zipfile import ZipFile
            from io import BytesIO

            r = requests.get(
                "https://gitlab.com/crossref/api-summaries/-/jobs/artifacts/main/download?job=annotate"
            )

            input_zip = ZipFile(BytesIO(r.content))

            for name in input_zip.namelist():
                if name.endswith("members.json"):
                    return input_zip.read(name)

        def _member_data(member_id):
            import requests

            CRAPI_URL = "https://api.crossref.org"
            USER_AGENT = "crossref_customer_id_exporter"

            try:
                res = requests.get(
                    f"{CRAPI_URL}/members/{member_id}",
                    headers={"User-Agent": USER_AGENT},
                )
            except Exception as e:
                raise e
            return res.json()["message"] if res.status_code == 200 else None

        def _search_admin_page(url, prefix, usr, pwd, role):
            import requests

            USER_AGENT = "crossref_customer_id_exporter"

            try:
                res = requests.request(
                    "POST",
                    url,
                    headers={
                        "Content-Type": "application/x-www-form-urlencoded",
                        "User-Agent": USER_AGENT,
                    },
                    data=f"usr={usr}&pwd={pwd}&username={usr}&role={role}"
                    f"&role={role}&password={pwd}&doi={prefix}&sf=search",
                )
            except Exception as e:
                raise e
            return res.text if res.status_code == 200 else None

        def _nth_table(table_num, html):
            from bs4 import BeautifulSoup as bs

            soup = bs(html, features="html.parser")
            return soup.findAll("table")[table_num]

        def _fill_empty_keys(keys):
            return [k or f"empty_{i}" for i, k in enumerate(keys)]

        def _table_header(table):
            from slugify import slugify

            return _fill_empty_keys([slugify(h.text) for h in table("th")])

        def _chunks(l: list, n: int) -> list:
            """Yield successive n-sized chunks from l."""
            for i in range(0, len(l), n):
                yield l[i : i + n]

        def _cell_content(cell):
            return (
                cell.text.strip()
                if cell.text
                else (
                    cell.find("input").get("value")
                    if cell.find("input")
                    else ""
                )
            )

        def _table_data(table, row_size):
            return list(
                _chunks(
                    [
                        _cell_content(cell)
                        for row in table("tr")
                        for cell in row("td")
                    ],
                    row_size,
                )
            )

        def _get_member_data(member_id, usr, pwd, role):
            # get prefixes for this member from the Crossref API
            member_data = _member_data(member_id)

            results = {}

            for prefix in member_data["prefixes"]:
                admin_page = _search_admin_page(
                    url="https://doi.crossref.org/servlet/publisherUserAdmin",
                    prefix=prefix,
                    usr=usr,
                    pwd=pwd,
                    role=role,
                )

                table = _nth_table(table_num=5, html=admin_page)
                header = _table_header(table)
                data = _table_data(table, len(header))

                for row in data:
                    if row[2] == prefix:
                        results[prefix] = row[1]

            return results

        def _get_secret(secret_name):
            """
            Get a secret from AWS
            :param secret_name: the secret name
            :return: the secret
            """
            bucket = "outputs.research.crossref.org"

            import claws.aws_utils
            import json

            connector = claws.aws_utils.AWSConnector(
                unsigned=False, bucket=bucket, region_name="us-east-1"
            )

            return json.loads(connector.get_secret(secret_name))

        from claws.aws_utils import AWSConnector
        from smart_open import open
        import json

        try:
            members: dict = json.loads(_get_member_data_file())

            bucket = "outputs.research.crossref.org"
            aws_connector = AWSConnector(
                bucket=bucket,
                unsigned=False,
            )

            crossref_secret = _get_secret("crossref_admin")

            from urllib.parse import quote

            pwd = quote(crossref_secret.get("CR_ADMIN_PWD"))
            usr = quote(
                crossref_secret.get(
                    "CR_ADMIN_USR",
                )
            )
            role = quote(crossref_secret.get("CR_ADMIN_ROLE"))

            for member in members:
                result = _get_member_data(
                    member_id=member.get("id", 0), usr=usr, pwd=pwd, role=role
                )

                s3_id = (
                    f"s3://{bucket}/annotations/members/"
                    f"{member.get('id', 0)}/publisher-ids.json"
                )

                with open(
                    s3_id,
                    "w",
                    transport_params={"client": aws_connector.s3_client},
                ) as output:
                    output.write(json.dumps(result))
                    output.flush()

                print(f"Data written to {s3_id}")

        finally:
            ...

    # tasks
    t1 = import_customers()

    t1


import_customer_ids()
