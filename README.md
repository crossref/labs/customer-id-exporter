# Customer ID Exporter
This is an Airflow script that scrapes the internal customer ID out of the admin console and loads it into the Labs API.

![license](https://img.shields.io/gitlab/license/crossref/labs/customer-id-exporter) ![activity](https://img.shields.io/gitlab/last-commit/crossref/labs/customer-id-exporter)

![AWS](https://img.shields.io/badge/AWS-%23FF9900.svg?style=for-the-badge&logo=amazon-aws&logoColor=white) ![Linux](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black) ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)

Scrapes the admin console to find the internal "publisher id" for each member and then pushes these as Labs API annotations.

&copy; Crossref 2024